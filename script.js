"use strict";

const calculator = () =>{
    const submitButton = document.querySelector('.btn');
    const serviceLevel = document.querySelectorAll('.tip');
    const result = document.querySelector('.result');
    let totalSum = 0;

    submitButton.addEventListener('click', (e) => {
        e.preventDefault(); // чтобы не перезагружало форму , пытается ее отправить
        let TotalBillAmount = document.querySelector('.amount').value;
        let NumberOfPersons = document.querySelector('.person').value;
        let tipActive;
        serviceLevel.forEach((tip) => {
            if(tip.checked){
                tipActive = tip.value;
            }
        });
        totalSum = Math.round(((TotalBillAmount * tipActive)/NumberOfPersons) * 100) / 100;
        result.innerHTML = totalSum;

    });
};

calculator();